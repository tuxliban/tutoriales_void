# Tutoriales para Void Linux

Este repositorio contendrá guías de ayuda para poder sacar un mejor provecho de esta maravillosa distribución de gnu/linux.
En lo posible se procurará tener en español el contenido que se vaya agregando.

# ÍNDICE
* Tutoriales
	* [Contenedor para ejecutar programas compilados con la librería C glibc en una instalación de Void con Musl](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Gu%c3%adas/Container%20Void/Guide.md)
	* [Guía de instalación mínima chroot](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Gu%c3%adas/Chroot.md)
	* [Guía Postinstall](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Gu%c3%adas/Postinstall.md)
	* [Guía instalación XBPS-static v21.1](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Gu%c3%adas/XBPS-static.md)
* Plantillas personalizadas
	* [Tutorial de cómo construir e instlar los paquetes](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/README.md)
	* [busybox-static](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/busybox-static)
	* [dash-static](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/dash-static)
	* [dwm-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/dwm-custom)
	* [ffmpeg-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/ffmpeg-custom)
	* [herbe](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/herbe)
	* [icecat(Pendiente de actualizar)](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/icecat)
	* [libreoffice](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/libreoffice-custom)
	* [linux4.14](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/linux4.14-custom)
	* [loksh-static](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/loksh-static)
	* [minimal-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/minimal-custom)
	* [moc-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/moc-custom)
	* [mplayer-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/mplayer-custom)
	* [nnn-static](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/nnn-static)
	* [oksh-static](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/oksh-static)
	* [palemoon](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/palemoon)
	* [cron](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/scron)
	* [dhcp](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/sdhcp)
	* [slstatus-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/slstatus-custom)
	* [st-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/st-custom)
	* [surf-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/surf)
	* [vlc-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/vlc-custom)
	* [xbps-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/xbps-custom)
	* [xbps-static-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/xbps-static-custom)
	* [xnotify-custom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/xnotify/template)
	* [zoom](https://git.disroot.org/tuxliban/tutoriales_void/src/branch/master/Plantillas/zoom/template)

