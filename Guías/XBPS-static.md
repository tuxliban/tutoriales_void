# **Guía para actualizar de versión al gestor de paquetes XBPS (versión estática)**

## Autores

### Telegram

* @tenshalito	Tuxliban Torvalds
* @lumaro	Luis

-----

## Introducción

A través del siguiente tutorial se mostrará cómo actualizar la versión de XBPS que está [incluída en Void Linux](https://github.com/void-linux/xbps) (v. 0.59.1) a la [versión 21.1](https://gitlab.com/xtraeme/xbps), distribuida y publicada recientemente por Juan Romero Pardines.

### ¿Qué es la vinculación estática?

En cualquier programa existe un enlace entre una llamada al método y la definición del método, pudiéndose realizar de forma estática o dinámica. En el enlace estático, el enlace se resuelve en el momento de la compilación, mientras que el enlace dinámico se resuelve en el tiempo de ejecución, siendo relativamente más lento. Por lo tanto, el enlace estático se traduce en un programa más rápido en comparación con el enlace dinámico y, al mismo tiempo, al contener las librerías necesarias para su funcionamiento, los ejecutables vinculados estáticamente son portátiles y a prueba de fallos frente a los cambios de ABI; por lo que se tiene la certeza de que el ejecutable seguirá funcionando en la misma arquitectura incluso con el paso de los años. Por último, los ejecutables vinculados estáticamente consumen menos memoria porque su tamaño binario es más pequeño y solo mapean las funciones de las que dependen en la memoria, caso contrario con las bibliotecas dinámicas.

### Interfaz binaria de aplicaciones (ABI)

ABI define las estructuras y los métodos que usará una aplicación compilada para acceder a la/s biblioteca/s externa/s a un nivel inferior al de una API. Una API define el orden en que pasan los argumentos a una función y qué funciones son parte de su biblioteca. La ABI define la mecánica de cómo se pasan estos argumentos (registros, pila, etc.) y cómo se almacena el código dentro del archivo de la biblioteca, de modo que cualquier programa que use la biblioteca pueda localizar la función deseada y ejecutarla. Si todo el sistema se ajusta a la misma ABI, entonces cualquier programa puede trabajar con cualquier archivo de biblioteca sin importar quién los creó. Cuando se produce un cambio de ABI, los programas que usan esa biblioteca no funcionarán a menos que se vuelvan a compilar para usar la nueva versión de la biblioteca.

### Musl libc

Antes de proceder con la construcción del paquete, cabe hacer una aclaración que la vinculación estática se hará tomando como base a la [libc Musl](https://www.musl-libc.org/faq.html) ya que esta libc evita con cuidado extraer grandes cantidades de código o datos que la aplicación no utilizará, por lo tanto, un código mínimo específico de la máquina significa menos posibilidades de rotura en arquitecturas minoritarias y un mayor éxito con el desarrollo en C "escribir una vez y ejecutarlo en todas partes".

Musl es una biblioteca estándar de C destinada a sistemas operativos Linux, desarrollada por Rich Felker con el objetivo de escribir una implementación libc limpia, eficiente y conforme a los estándares ISO C y POSIX, además de extensiones comunes, permitiendo un enlazado estático de bibliotecas eficaz y robusto.

## Modificación de plantilla

Como se mencionó anteriormente, para actualizar la versión del gestor de paquetes XBPS se usará la versión vinculada estáticamente. Para nuestro fin usaremos la plantilla distribuida por Void en la que realizaremos unos pequeños cambios que se mencionarán a continuación:

| **Versión original (github)** | **Versión actualizada** |
| :--- | :--- |
| version=0.59.1 | version=21.1 |
| revision=5 | revision=1 |
| homepage="https://github.com/void-linux/xbps" | homepage="https://gitlab.com/xtraeme/xbps/" |
| changelog="https://github.com/void-linux/xbps/blob/master/NEWS" | changelog="${homepage}-/blob/master/NEWS.md" |
| distfiles="https://github.com/void-linux/xbps/archive/${version}.tar.gz" | distfiles="${homepage}-/archive/${version}/xbps-${version}.tar.gz" |
| checksum=0cbd8d5f23a62047c75974bca21da9f004a94efffd7f37c68562a8dbc869fb2a | checksum=51a8ab75ece07bea64ea325e2c62314f9477bab06de297e8d3577f0fb4f48578 |
| vlicense LICENSE.3RDPARTY | vlicense 3RDPARTY |

Una vez modificada la plantilla guardar cambios y proceder con la construcción del paquete usando *xbps-src*

	$ ./xbps-src pkg xbps-static

**Nota:** En caso de tener un sistema con la [libc Glibc](https://www.gnu.org/software/libc/), será necesario construir el paquete usando la compilación cruzada, para ello seguir con los siguiente pasos:

	$ ./xbps-src -a x86_64 pkg xbps-static

Cuando el paquete termine de construirse proceder a instalarlo. Se recomienda ampliamente usar el método a través del comando *xi*

**Nota:** Si el comando *xi* no aparece en el PATH significa que no está instalado, de ser así instale el paquete `xtools`

	$ xi xbps-static

Durante el proceso de desempaquetado aparece un mensaje indicando que la función `xbps-fbulk.static` es removida. Esto se debe a que para la nueva versión de XBPS en su lugar utiliza [go-xbulk](https://gitlab.com/xtraeme/go-xbulk) para la construcción en tiempo real de un árbol de dependencias.

Ahora para corroborar que ya se tiene en uso la versión más reciente basta con escribir lo siguiente en la terminal:

	$ xbps-query.static -V

Al introducir lo anterior deberá devolver lo siguiente:

	XBPS: 21.1 API: 20211203 GIT: UNSET https://gitlab.com/xtraeme/xbps

Hasta este punto, en arquitecturas **x86_64** XBPS ya es funcional, sin embargo, para la arquitectura de **x86_64-musl** existe un pequeño problema al querer sincronizar los repositorios que consiste en que el sistema muestra un error con la verificación del certificado de seguridad y otro más que no detecta correctamante la arquitectura del sistema:

1. Error de verificación de certificado de seguridad:

```
# xbps-install -S
[*] Updating repository 'https://repo-us.voidlinux.org/current/musl/x86_64-repodata' ...
Certificate verification failed for /C=US/O=Internet Security Research Group/CN=ISRG Root X1
SSL_connect returned 1
ERROR: [reposync] failed to fetch file 'https://repo-us.voidlinux.org/current/musl/x86_64-repodata': Operation not permitted
```

2. Error de detección incorrecta de architectura (debería de ser *x86_64-musl*)

```
# xbps-install -Sd
[DEBUG] Architecture: x86_64
```

Para poder solucionar los dos problemas previamente mencionados realizar lo siguiente:

1. Para el problema de verificación de certificado se añadirá una variable de entorno en el archivo de perfil de inicio de sesión `.profile`:

```
$ echo "SSL_CA_CERT_FILE=/etc/ssl/certs.pem" >> "$HOME"/.profile
```

2. Para el problema de la detección correcta de arquitectura también se añadirá otra variable de entorno al perfil de inicio de sesión `.profile`:

```
$ echo "XBPS_ARCH=x86_64-musl" >> "$HOME"/.profile
```

**Nota:** Si utilizan la versión libc de *Glibc* entonces la variable de entorno debe quedar como `XBPS_ARCH=x86_64`

**Nota2:** Es posible que aún con estas modificaciones persista el error, eso puede ser debido, en el caso de utilizar *doas* para el escalado de privilegios de usuario y no mantener las variables de entorno definidas en el `.profile` del shell de usuario. Para ello, podemos establecer la siguiente configuración en en fichero `/etc/doas.conf`

```
permit nopass keepenv :wheel
```


3. Si tienen instalada la versión de XBPS vinculada dinámicamente pueden eliminarla; si tenían la versión de XBPS vinculada estáticamente pueden omitir este paso.

```
# xbps-remove -R xbps
```

4. Como la mayoría de usuarios están acostubrados a utilizar la versión de XBPS vinculada dinámicamente, se les hará extraño estar llamando a las funciones de XBPS como `xbps-install.static xbps-remove.static`, etc, por lo tanto, para seguir usándolo de la manera habitual bastará con crear unos enlaces simbólicos de cada una de las funciones de XBPS e incluirlos en nuestro corresponiente PATH. Para esto, se puede optar por tres opciones posibles:

	* Utilizar el PATH que el sistema utiliza para instalar binarios: `/usr/bin`
	* Usar el PATH que se utiliza cuando se compila software adicional a través de *make*: `/usr/local/bin`
	* Usar un PATH local que utilice el usuario: `"$HOME"/.local/bin`. En caso de no existir el directorio crearlo: `$ mkdir -p "$HOME"/.local/bin`

Una vez decidido qué PATH utilizán, proceder a crear los enlaces simbólicos. Para nuestro ejemplo tomaremos el tercer supuesto:

```
$ ln -s /usr/bin/xbps-alternatives.static "$HOME"/.local/bin/xbps-alternatives
$ ln -s /usr/bin/xbps-checkvers.static "$HOME"/.local/bin/xbps-checkvers
$ ln -s /usr/bin/xbps-create.static "$HOME"/.local/bin/xbps-create
$ ln -s /usr/bin/xbps-dgraph.static "$HOME"/.local/bin/xbps-dgraph
$ ln -s /usr/bin/xbps-digest.static "$HOME"/.local/bin/xbps-digest
$ ln -s /usr/bin/xbps-fetch.static "$HOME"/.local/bin/xbps-fetch
$ ln -s /usr/bin/xbps-install.static "$HOME"/.local/bin/xbps-install
$ ln -s /usr/bin/xbps-pkgdb.static "$HOME"/.local/bin/xbps-pkgdb
$ ln -s /usr/bin/xbps-query.static "$HOME"/.local/bin/xbps-query
$ ln -s /usr/bin/xbps-reconfigure.static "$HOME"/.local/bin/xbps-reconfigure
$ ln -s /usr/bin/xbps-remove.static "$HOME"/.local/bin/xbps-remove
$ ln -s /usr/bin/xbps-rindex.static "$HOME"/.local/bin/xbps-rindex
$ ln -s /usr/bin/xbps-uchroot.static "$HOME"/.local/bin/xbps-uchroot
$ ln -s /usr/bin/xbps-uhelper.static "$HOME"/.local/bin/xbps-uhelper
``` 

5. Ahora que se han realizado los cambios, lo que resta es reiniciar la sesión y *loguearse* nuevamente para que las modificaciones surtan efecto:

```
$ pkill Xorg 
```

6. Ahora podrán utilizar con completa libertad la versión más reciente de XBPS y en caso de querer actualizar a otra versión liberada por xtraeme bastará con modificar las filas que corresponden a los campos `version` y `checksum` por las que correspondan.

Para finalizar, la lista de cambios que xtraeme realice en XBPS puede ser consultada [aquí](https://gitlab.com/xtraeme/xbps/-/blob/master/NEWS.md).

## Referencias

* Juan RP (s.f.) xbps. Sitio web de Github: https://github.com/void-linux/xbps
* musl libc (s.f.) Introduction to musl. Sitio web de musl-libc.org: https://www.musl-libc.org/intro.html
* xtraeme (s.f.) go-xbulk. Sitio web de Gitlab: https://gitlab.com/xtraeme/go-xbulk
* xtraeme (s.f.) xbps. Sitio web de Gitlab: https://gitlab.com/xtraeme/xbps
* Wikipedia (s.f.) https://es.wikipedia.org
